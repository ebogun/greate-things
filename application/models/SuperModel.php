<?php

class Application_Model_SuperModel extends Zend_Db_Table_Abstract
{

    public function getRecord($id)
    {
        $id = (int)$id;
        $row = $this->fetchRow('id = ' . $id);
        if(!$row) {
            throw new Exception("Нет записи с id - $id");
        }
        return $row->toArray();
    }

    public function addRecord(Array $params)
    {
        $this->insert($params);
    }

    public function editRecord($id, $params)
    {
        $this->update($params, 'id = ' . (int)$id);
    }

    public function deleteRecord($id)
    {
        $this->delete('id = ' . (int)$id);
    }
}

