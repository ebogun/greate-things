<?php
/**
 * Class Application_Model_DbTable_Task
 * @property string $text
 * @property int $category_id
 * @property int $status
 */
class Application_Model_DbTable_Task extends Application_Model_SuperModel
{
    const STATUS_NOT_DONE = 0;
    const STATUS_DONE = 1;

    static $statusList = array(
        self::STATUS_NOT_DONE => 'Not Done',
        self::STATUS_DONE => 'Done',
    );

    protected $_name = 'task';

    public $text;
    public $status;
    public $category_id;
    public $order;

    public function getStatusName(){
        try{
            return self::$statusList[$this->status];
        } catch(Zend_Exception $e){
            echo "Caught exception: " . get_class($e) . "\n";
            echo "Message: " . $e->getMessage() . "\n";
        }
    }

    public function attachFields($records = null){
        if(empty($records))
            $records = $this->getRecord($this->id);

        foreach($records as $field => $data){
            if(property_exists(__CLASS__, $field)){
                $this->$field = $data;
            }
        }
    }

    public function search(){
        $select = $this->select();
        if($this->text != '')
            $select->where('text like ?', '%'.$this->text.'%');
        if($this->status != '')
            $select->where('status = ?', $this->status);
        if(!empty($this->category_id))
            $select->where('category_id = ?', $this->category_id);

        $select->order('order asc');

        return $this->fetchAll($select);
    }

    public static function load($id){
        $task = new Application_Model_DbTable_Task();
        $task->id = $id;
        $task->attachFields();
        return $task;
    }

    public static function getAssocStatusList(){
        return array('' => 'Select') + self::$statusList;
    }

    public static function updateOrder($orderAray){
        $task = new Application_Model_DbTable_Task();
        $i = 0;
        foreach($orderAray as $order){
            $task->editRecord($order, array('order' => ++$i));
        }
    }
}

