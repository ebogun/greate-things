<?php

class Application_Model_DbTable_Category extends Application_Model_SuperModel
{
    protected $_name = 'category';

    public static function getName($id){
        if(!empty($id)){
            $category = new Application_Model_DbTable_Category();
            $record = $category->getRecord($id);
            $name = $record['name'];
        } else {
            $name = 'None';
        }
        return $name;
    }

    public static function getAssocList(){
        $categories = new self();
        $data = array();
        foreach($categories->fetchAll()->toArray() as $value){
            $data[$value['id']] = $value['name'];
        }
        return array('' => 'Select') + $data;
    }
}

