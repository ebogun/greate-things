<?php

class CategoryController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $categories = new Application_Model_DbTable_Category();
        $this->view->categories = $categories->fetchAll();
    }

    public function addAction()
    {
        $form = new Application_Form_Category();
        $form->submit->setLabel("Добавить");
        $this->view->form = $form;
        if($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();
            if($form->isValid($formData)){
                $name = $form->getValue('name');
                $category = new Application_Model_DbTable_Category();
                $category->addRecord(array('name' => $name));
                $this->_helper->redirector('index');
            } else {
                $form->populate($formData);
            }
        }
    }

    public function editAction()
    {
        $form = new Application_Form_Category();
        $form->submit->setLabel("Сохранить");
        $this->view->form = $form;
        if($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();
            if($form->isValid($formData)){
                $id = (int)$form->getValue('id');
                $name = $form->getValue('name');
                $category = new Application_Model_DbTable_Category();
                $category->editRecord($id, array('name' => $name));
                $this->_helper->redirector('index');
            } else {
                $form->populate($formData);
            }
        } else {
            $id = $this->_getParam('id', 0);
            if($id > 0) {
                $category = new Application_Model_DbTable_Category();
                $form->populate($category->getRecord($id));
            }
        }
    }

    public function deleteAction()
    {
        if($this->getRequest()->isGet()){
            $id = $this->_getParam('id');
            $category = new Application_Model_DbTable_Category();
            $category->deleteRecord($id);
            $this->_helper->redirector('index');
        }
    }

}





