<?php

class IndexController extends Zend_Controller_Action
{

    public function init() {
        //проверяем, используется ли AJAX
        if ($this->getRequest()->isXmlHttpRequest()) {
            //если AJAX - отключаем авторендеринг шаблонов
            Zend_Controller_Action_HelperBroker::removeHelper('viewRenderer');
        }
    }

    public function indexAction()
    {
        $task = new Application_Model_DbTable_Task();
        if($this->getRequest()->isPost()){
            $task->attachFields($this->_getParam('Task'));
        }
        $this->view->tasks = $task->search();
        $this->view->filter = $task;
    }

    public function addAction()
    {
        $form = new Application_Form_Task();
        $form->submit->setLabel("Add");
        $this->view->form = $form;
        if($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();
            if($form->isValid($formData)){
                $text = $form->getValue('text');
                $category_id = $form->getValue('category_id');
                $status = $form->getValue('status');
                $task = new Application_Model_DbTable_Task();
                $task->addRecord(array('text' => $text, 'category_id' => $category_id, 'status' => $status));
                $this->_helper->redirector('index');
            } else {
                $form->populate($formData);
            }
        }
    }

    public function editAction()
    {
        $form = new Application_Form_Task();
        $form->submit->setLabel("Save");
        $this->view->form = $form;
        if($this->getRequest()->isPost()){
            $formData = $this->getRequest()->getPost();
            if($form->isValid($formData)){
                $id = (int)$form->getValue('id');
                $text = $form->getValue('text');
                $category_id = $form->getValue('category_id');
                $status = $form->getValue('status');
                $task = new Application_Model_DbTable_Task();
                $task->editRecord($id, array('text' => $text, 'category_id' => $category_id, 'status' => $status));
                $this->_helper->redirector('index');
            } else {
                $form->populate($formData);
            }
        } else {
            $id = $this->_getParam('id', 0);
            if($id > 0) {
                $task = new Application_Model_DbTable_Task();
                $form->populate($task->getRecord($id));
            }
        }
    }

    public function deleteAction()
    {
        if($this->getRequest()->isGet()){
            $id = $this->_getParam('id');
            $task = new Application_Model_DbTable_Task();
            $task->deleteRecord($id);
            $this->_helper->redirector('index');
        }
    }

    public function orderAction()
    {
        $order = $this->_getParam('order');
        Application_Model_DbTable_Task::updateOrder($order);
    }


}









