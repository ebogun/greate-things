<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    public function _initNavigation()
    {
        $this->bootstrapView();
        $view = $this->getResource('view');

        $pages = array(
            array(
                'controller' => 'index',
                'label'         => _('Tasks'),
            ),
            array(
                'controller' => 'category',
                'action'        => 'index',
                'label'         => _('Category'),
            ),
        );

        $container = new Zend_Navigation($pages);
        $view->menu = $container;

        return $container;
    }
}

