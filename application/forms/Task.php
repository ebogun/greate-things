<?php

class Application_Form_Task extends Zend_Form
{
    public function init()
    {
        $this->setName('task');
        $id = new Zend_Form_Element_Hidden('id');
        $id->addFilter('Int');
        $isEmptyMessage = 'Значение является обязательным и не может быть пустым';

        $name = new Zend_Form_Element_Textarea('text');
        $name->setLabel('Text')
            ->setAttrib('rows', 4)
            ->setRequired(true)
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty', true,
                array('messages' => array('isEmpty' => $isEmptyMessage))
            )->setDecorators(array(
                'ViewHelper',
                'Errors',
                //array(array('data' => 'HtmlTag'), array('tag' => false)),
                'Label',
                array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group')),
            ));

        $category = New Zend_Form_Element_Select('category_id');
        $category->setLabel('Category:')
            ->setRequired(true)
            ->addValidator('NotEmpty', true,
                array('messages' => array('isEmpty' => $isEmptyMessage))
            )->addMultiOptions(
                Application_Model_DbTable_Category::getAssocList()
            )->setDecorators(array(
                'ViewHelper',
                'Errors',
                //array(array('data' => 'HtmlTag'), array('tag' => false)),
                'Label',
                array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group')),
            ));

        $status = New Zend_Form_Element_Select('status');
        $status->setLabel('Status:')
            ->setRequired(true)
            ->addValidator('NotEmpty', true,
                array('messages' => array('isEmpty' => $isEmptyMessage))
            )->addMultiOptions(
                Application_Model_DbTable_Task::getAssocStatusList()
            )->setDecorators(array(
                'ViewHelper',
                'Errors',
                //array(array('data' => 'HtmlTag'), array('tag' => false)),
                'Label',
                array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group')),
            ));

        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setAttrib('id', 'submitbutton')->setAttrib('class','btn btn-default');
        $this->addElements(array($id, $name, $category, $status, $submit));
    }
}

