<?php

class Application_Form_Category extends Zend_Form
{
    public function init()
    {
        $this->setName('category');
        $id = new Zend_Form_Element_Hidden('id');
        $id->addFilter('Int');
        $isEmptyMessage = 'Значение является обязательным и не может быть пустым';

        $title = new Zend_Form_Element_Text('name');
        $title->setLabel('Название')
            ->setRequired(true)
            ->addFilter('StripTags')
            ->addFilter('StringTrim')
            ->addValidator('NotEmpty', true,
                array('messages' => array('isEmpty' => $isEmptyMessage))
            )->setDecorators(array(
                'ViewHelper',
                'Errors',
                //array(array('data' => 'HtmlTag'), array('tag' => false)),
                'Label',
                array(array('row' => 'HtmlTag'), array('tag' => 'div', 'class' => 'form-group')),
            ));

        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setAttrib('id', 'submitbutton')->setAttrib('class','btn btn-default');
        $this->addElements(array($id, $title, $submit));
    }

}

